package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PropertyDetailsPage {

    private WebDriver driver;

    private By showAmenitiesButton = By.xpath("//div[@class='_1tv4hg3']/a");
    private By verifyPoolFilter = By.xpath("//div[@class='_1dotkqq']");
    private By facilities = By.xpath("//div[@class='_aujnou']");

    public PropertyDetailsPage(WebDriver driver){
        this.driver = driver;
    }

    public void clickShowAmenitiesButton(){
        waitForLoad(driver);
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(showAmenitiesButton));
        driver.findElement(showAmenitiesButton).click();
    }

    public boolean verifyPoolIsDisplayedOnAmenitiesPopUp() throws InterruptedException {
        waitForLoad(driver);

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(facilities));

        int amenitiesCount =driver.findElements(facilities).size();
        int desireAmenitiesIndex= -1;


        for (int i=0 ; i<amenitiesCount ; i++){
            String text = driver.findElements(facilities).get(i).findElements(By.xpath("//div[@class='_1crk6cd'][1]")).get(i).getText();
            if (text.contains("facilities")){
                desireAmenitiesIndex=i + 1;
                break;
            }
        }

        By updatedFacilities = By.xpath("//div[@class='_aujnou']['"+desireAmenitiesIndex +"']");
        WebElement facilityDriver = driver.findElement(updatedFacilities);

        int count =driver.findElements(verifyPoolFilter).size();

        for (int i=0; i< count; i++){
            String text = facilityDriver.findElements(verifyPoolFilter).get(i).getText();
            if(text.equalsIgnoreCase("Pool")){

                return true;
            }
        }
        return false;
    }


    public void waitForLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(pageLoadCondition);
    }
}
