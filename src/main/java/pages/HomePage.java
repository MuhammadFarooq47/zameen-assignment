package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.html5.ApplicationCache;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class HomePage {
    private WebDriver driver;
    private By Location = By.id("bigsearch-query-detached-query");
    private By currentDate = By.cssSelector("div[class='_xgnsug'] div[class='_f8btejl']");
    private By checkInCalenderBlock = By.cssSelector("div[data-testid='structured-search-input-field-dates-panel']");
    private By checkOutCalenderBlock = By.cssSelector("div[data-testid='structured-search-input-field-dates-panel']");
    private By checkOutDates = By.cssSelector("td[aria-disabled='false'] div[class='_f8btejl']");
    private By guests = By.cssSelector("div[data-testid='structured-search-input-field-guests-button']");
    private By addAdults = By.cssSelector("button[data-testid='stepper-adults-increase-button']");
    private By addChilds = By.xpath("//button[@data-testid='stepper-children-increase-button']");
    private By seacrhButton = By.xpath("//button[@data-testid='structured-search-input-search-button']");
    private By filters = By.className("_1snxcqc");
    private By propertyResult = By.cssSelector("div[class='_8ssblpx']");
    private By moreFiltersButton = By.cssSelector("div[id='menuItemButton-dynamicMoreFilters'] button");
    private By addBedroomButton = By.xpath("(//div[@class='_jwbbkz'])[3]/div/div[2]/button[2]");
    private By showStaysButton = By.cssSelector("button[data-testid='more-filters-modal-submit-button']");
    private By bedroomCounts = By.xpath("(//div[@class='_1665lvv'])[2]/span[1]");
    private By closeMoreFiltersPopupButton = By.className("_187sg6v");
    private By firstPropertyLink = By.xpath("(//div[@class='_8ssblpx'])[1]");
    private By showAmenitiesButton = By.xpath("//div[@class='_1tv4hg3']/a");
    private By selectPoolCheckbox = By.id("filterItem-facilities-checkbox-amenities-7");
    private By verifyPoolFilter = By.xpath("//div[@class='_1dotkqq']");
    private By facilities = By.xpath("//div[@class='_aujnou']");
    private By propertyOnMap=By.xpath("//div[@class='gm-style-pbc']/following-sibling::div/div/div[4]/div[3]/div/button/div/div");
    private By buttonOfPropertyOnMap=By.xpath("//div[@class='gm-style-pbc']/following-sibling::div/div/div[4]/div[3]/div/button");
    private By ratingList = By.xpath("(//div[@class='_vaj62s']/span)[1]");
    private By ratingMap = By.xpath("//div[@class='_1c50qo6']/span");
    private By nameList = By.xpath("(//div[@class='_bzh5lkq'])[1]");
    private By nameMap = By.xpath("//div[@class='_1x0fg6n']/div[2]/following-sibling::div");
    private By priceList = By.xpath("(//div[@class='_1fwiw8gv']/span)[1]");
    private By priceMap = By.xpath("//div[@class='_1q6k59c']/div[2]/div[4]/div/span");




    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public void setLocation(String searchLocation, String selectLocation) throws InterruptedException {
        waitForLoadHomePage(driver);
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.findElement(Location).click();
        WebElement element = driver.findElement(Location);

        driver.findElement(Location).sendKeys(searchLocation);



        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='_1sx4f1vv']")));
        driver.findElement(Location).sendKeys(Keys.DOWN);

        JavascriptExecutor js = (JavascriptExecutor)driver;
        String str = "return document.getElementById(\"bigsearch-query-detached-query\").value;";

        String item = (String)js.executeScript(str);

        int i=0;
        while(!item.contains(selectLocation)){
            if(item.equals(searchLocation)){
                i++;
                break;
            }
            driver.findElement(By.id("bigsearch-query-detached-query")).sendKeys(Keys.DOWN);
            item = (String) js.executeScript(str);

        }
        if (i == 0){
            driver.findElement(Location).sendKeys(Keys.ENTER);
        }
    }

    public String setCheckInDate(){
        WebDriverWait wait = new WebDriverWait(driver,20);

        wait.until(ExpectedConditions.visibilityOfElementLocated(checkInCalenderBlock));
        WebElement checkInDriver = driver.findElement(checkInCalenderBlock);
        String setInDate = checkInDriver.findElements(currentDate).get(8).getText();
        checkInDriver.findElements(currentDate).get(8).click();
        return setInDate;
    }

    public String setCheckOutDate(){
        WebElement checkOutDriver = driver.findElement(checkOutCalenderBlock);
        String setOutDate = checkOutDriver.findElements(currentDate).get(15).getText();
        checkOutDriver.findElements(checkOutDates).get(15).click();
        return setOutDate;
    }

    public int setGuests(int adultsCount, int childCount){
        int count=0;
        driver.findElement(guests).click();
        for (int i=0 ; i < adultsCount ; i++){
            driver.findElement(addAdults).click();
            count++;
        }

        for (int j=0 ; j < childCount ; j++){
            driver.findElement(addChilds).click();
            count++;
        }
        return count;
    }

    public void clickOnSearchButton(){
        driver.findElement(seacrhButton).click();
    }

    public String getCheckInDate(){
        String[] checkInDate = driver.findElement(filters).getText().split(" ");
        return checkInDate[4];
    }

    public String getCheckOutDate(){
        String[] checkOutDate = driver.findElement(filters).getText().split(" ");
        int count = checkOutDate.length;
        return checkOutDate[count-4];
    }

    public int getGuestCount(){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(filters));
        String[] guestCount = driver.findElement(filters).getText().split(" ");
        int count = guestCount.length;

        return Integer.parseInt(guestCount[count-2]);
    }

    public boolean verifyNumberOfGuestsOnList(){
        String[] guest = driver.findElement(filters).getText().split(" ");
        int guestLength = guest.length;
        int selectedGuest = Integer.parseInt(guest[guestLength-2]);
        WebElement firstListDriver;
        WebElement secondListDriver;
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(propertyResult));
        int count = driver.findElements(propertyResult).size();

        if(count == 10){
            return whenListInTwoParts(selectedGuest);
        }
        else{
            for (int i=0 ; i<count ; i++){
                String[] chunks=driver.findElements(propertyResult).get(i).findElements(By.xpath("//div[@class='_kqh46o'][1]")).get(i).getText()
                        .split(" ");
                if (Integer.parseInt(chunks[0]) < selectedGuest){
                    return false;
                }
            }
        }
        return true;
    }

    public void clickMoreFilters(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(moreFiltersButton));
        driver.findElement(moreFiltersButton).click();
    }

    public void addBedrooms(int bedroomCount){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(addBedroomButton));

        wait.until(ExpectedConditions.elementToBeClickable(addBedroomButton));
        for(int i=0; i<bedroomCount; i++){

            driver.findElement(addBedroomButton).click();
            wait.until(ExpectedConditions.elementToBeClickable(addBedroomButton));
        }
    }

    public void selectPoolCheckbox(){
        driver.findElement(selectPoolCheckbox).click();
    }

    public void clickShowStays(){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(showStaysButton));
        driver.findElement(showStaysButton).click();
    }

    public boolean verifyNumberOfBedroomsOnList() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        clickMoreFilters();
        wait.until(ExpectedConditions.visibilityOfElementLocated(bedroomCounts));
        int bedroomCount = Integer.parseInt(driver.findElement(bedroomCounts).getText());
        wait.until(ExpectedConditions.elementToBeClickable(closeMoreFiltersPopupButton));
        driver.findElement(closeMoreFiltersPopupButton).click();

        waitForLoadHomePage(driver);

        wait.until(ExpectedConditions.visibilityOfElementLocated(propertyResult));
        int count = driver.findElements(propertyResult).size();

        if(count == 10){
            return whenListInTwoParts(bedroomCount);
        }
        else {
            for (int i = 0; i < count; i++) {
                String[] chunks = driver.findElements(propertyResult).get(i).findElements(By.xpath("//div[@class='_kqh46o'][1]")).get(i).getText()
                        .split(" ");
                if (Integer.parseInt(chunks[0]) < bedroomCount) {
                    return false;
                }
            }
        }
        return true;
    }

    public PropertyDetailsPage openFirstProperty(){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.elementToBeClickable(firstPropertyLink));
        driver.findElement(firstPropertyLink).click();
        Set<String> ids = driver.getWindowHandles();
        Iterator<String> it =ids.iterator();
        String parentID = it.next();
        String childID = it.next();
        driver.switchTo().window(childID);
        return new PropertyDetailsPage(driver);
    }

    public void hoverOverFirstProperty(){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(firstPropertyLink));

        Actions action = new Actions(driver);
        WebElement firstProperty = driver.findElement(firstPropertyLink);
        action.moveToElement(firstProperty).build().perform();
    }

    public boolean checkPropertyColourOnMap(){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(propertyOnMap));


        String[] color = driver.findElement(propertyOnMap).getCssValue("color").substring(5,18).split(", ");

        if(color[0].equalsIgnoreCase("255") && color[1].equalsIgnoreCase("255") && color[2].equalsIgnoreCase("255") ){
            return true;
        }
        else
            return false;
    }

    public void clickPropertyOnMap(){
        WebDriverWait wait = new WebDriverWait(driver , 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(buttonOfPropertyOnMap));
        driver.findElement(buttonOfPropertyOnMap).click();
    }

    public boolean detailsVerificationOnListAndMap(){
        WebDriverWait wait = new WebDriverWait(driver , 40);

        wait.until(ExpectedConditions.visibilityOfElementLocated(ratingList));
        wait.until(ExpectedConditions.visibilityOfElementLocated(ratingMap));
        wait.until(ExpectedConditions.visibilityOfElementLocated(nameList));
        wait.until(ExpectedConditions.visibilityOfElementLocated(nameMap));
        wait.until(ExpectedConditions.visibilityOfElementLocated(priceList));
        wait.until(ExpectedConditions.visibilityOfElementLocated(priceMap));

        String ratingTextOfPropertyOnList = driver.findElement(ratingList).getText();
        String ratingTextOfPropertyOnMap = driver.findElement(ratingMap).getText();
        String nameTextOfPropertyOnList = driver.findElement(nameList).getText();
        String nameTextOfPropertyOnMap = driver.findElement(nameMap).getText();
        String priceTextOfPropertyOnList = driver.findElement(priceList).getText();
        String priceTextOfPropertyOnMap = driver.findElement(priceMap).getText();

        if(!ratingTextOfPropertyOnList.equalsIgnoreCase(ratingTextOfPropertyOnMap)){
            return false;
        }

        else if(!nameTextOfPropertyOnList.equalsIgnoreCase(nameTextOfPropertyOnMap)){
            return false;
        }

        else if(!priceTextOfPropertyOnList.equalsIgnoreCase(priceTextOfPropertyOnMap)){
            return false;
        }
        else
            return true;

    }

    public boolean whenListInTwoParts(int selectedGuest){
        WebElement firstListDriver;
        WebElement secondListDriver;
        firstListDriver = driver.findElement(By.xpath("//div[@itemprop='itemList']/div[2]"));
        secondListDriver = driver.findElement(By.xpath("//div[@itemprop='itemList']/div[4]"));
        int size1 =firstListDriver.findElements(propertyResult).size();
        int size2 =secondListDriver.findElements(propertyResult).size();

        for (int i=0; i<size1 ; i++){
            String[] chunks=firstListDriver.findElements(propertyResult).get(i).findElements(By.xpath("//div[@class='_kqh46o'][1]")).get(i).getText()
                    .split(" ");
            if (Integer.parseInt(chunks[0]) < selectedGuest){
                return false;
            }
        }
        int j=10;
        for (int i=0; i<size2 ; i++){
            String[] chunks=secondListDriver.findElements(propertyResult).get(i).findElements(By.xpath("//div[@class='_kqh46o'][1]")).get(j).getText()
                    .split(" ");
            if (Integer.parseInt(chunks[0]) < selectedGuest){
                return false;
            }
            j++;
        }
        return true;
    }

    public void waitForLoadHomePage(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(pageLoadCondition);
    }

}
