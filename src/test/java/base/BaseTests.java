package base;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.HomePage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTests {
    private WebDriver driver;
    protected HomePage homePage;
    public String actualCheckInDate;
    public String actualCheckOutDate;
    public Properties prop;


    @BeforeMethod
    public void setUp() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver" , "resources/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        driver=new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        prop = new Properties();
        InputStream input;

        try {
            input = new FileInputStream("properties/config.properties");
            prop.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }


        driver.get(prop.getProperty("URL"));
        driver.manage().window().maximize();

        homePage = new HomePage(driver);

        homePage.setLocation(prop.getProperty("searchLocation"),prop.getProperty("selectLocation"));
        actualCheckInDate = homePage.setCheckInDate();
        actualCheckOutDate = homePage.setCheckOutDate();
        int actualGuests =homePage.setGuests(Integer.parseInt(prop.getProperty("AdultCount")),Integer.parseInt(prop.getProperty("ChildCount")));
        homePage.clickOnSearchButton();

    }

    @AfterMethod
    public void tearDown(){
       driver.quit();
    }
}
