package searchCriteria;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.PropertyDetailsPage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchCriteriaTests extends BaseTests {



    @Test (priority=1)
    public void verifySearchCriteria() throws InterruptedException {
        assertEquals(actualCheckInDate, homePage.getCheckInDate());
        assertEquals(actualCheckOutDate, homePage.getCheckOutDate());
        assertEquals((Integer.parseInt(prop.getProperty("AdultCount")) + Integer.parseInt(prop.getProperty("ChildCount"))),homePage.getGuestCount());
        assertTrue(homePage.verifyNumberOfGuestsOnList());
    }

    @Test (priority=2)
    public void verifyExtraFiltersProjection() throws InterruptedException {
        homePage.clickMoreFilters();
        homePage.addBedrooms(Integer.parseInt(prop.getProperty("BedroomCount")));
        homePage.selectPoolCheckbox();
        homePage.clickShowStays();
        assertTrue(homePage.verifyNumberOfBedroomsOnList());
        PropertyDetailsPage pdp = homePage.openFirstProperty();
        pdp.clickShowAmenitiesButton();
        assertTrue(pdp.verifyPoolIsDisplayedOnAmenitiesPopUp());
    }

    @Test (priority=3)
    public void verifyPropertyDisplayOnMap(){
        homePage.hoverOverFirstProperty();
        assertTrue(homePage.checkPropertyColourOnMap());
        homePage.clickPropertyOnMap();
        assertTrue(homePage.detailsVerificationOnListAndMap());
    }

}
